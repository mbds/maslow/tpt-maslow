# TPT MASLOW : Ce repo regroupe l'ensemble des briques du projet

MASLOW rend la vie meilleure, améliore la qualité, réduit la consommation, améliore l'efficacité de la gestion, les économies de coûts et le monitoring.

L'objectif de ce projet est de mettre en place une maison intelligente. Une maison intelligente désigne une installation domestique pratique où les appareils et les dispositifs peuvent être automatiquement contrôlés à distance, de n'importe où, grâce à une connexion internet utilisant un appareil mobile ou un autre dispositif en réseau. Les appareils d'une maison intelligente sont interconnectés par l'internet, ce qui permet à l'utilisateur de contrôler à distance des fonctions telles que la sécurité de l'accès à la maison, la température et l'éclairage.

Etudiante :
*   BOUKADIDA Imen

## Architecture de la stack

![Stack Docker](https://gitlab.com/mbds/maslow/tpt-maslow/-/blob/master/docker_stack.PNG)

## Installation à faire avant de commencer :

1. Installer openHab :
    - Suivre les instructions de ce tutos : https://www.openhab.org/docs/installation/windows.html
    - Une fois l'installation est finie, lancer le fichier intitulé "start" qui se trouve dans le répertoire openHab
    - Dans votre navigateur préféré taper :"http://localhost:8080/" pour compléter la configuration de openHab, ça peut prendre quelques minutes...
    - "Bienvenue dans openHAB 2" est affiché ?, votre configuration est finie et maintenant on passe à la configuration des objets connectés
    - Choisir la rubrique "Paper UI" -> "Configuration" -> "Binding" -> "+" -> "Bindings" -> dans la barre de recherche taper : "Wemo Binding" -> cliquer sur "Install" 
    - Dans la rubrique Configuration -> Things appuier sur le "+" -> "WeMo Binding"  -> selectionner l'objet afficher puis cliquer sur "Add as Object"
    - L'objet est bien été intégré à openHab, pour consulter ses items et modifier les détails de cet objet: cliquer sur Configuration -> Things
    - Cliquer sur l'objet affiché -> Channels -> séléctionner les channels(items)
    - Maintenant, aller à l'adresse "http://localhost:8080/start/index" -> Habpanel
    - La page d'accueil du Habpanel s'affiche, cliquer sur le bouton en haut à droite "Edit dashbords" -> "Nouveau Tableau de bord" -> Taper un nom exemple : "Cuisine". Démarches pour créer les différentes pièces de la maison.
    - Cliquer sur le tableau de bord créé -> Cliquer sur le petit crayon à côté du titre -> "Ajout des widgets" -> choisir le type "Switch" pour la prise connectée -> configurer votre prise connectée en lui donnant un Nom, item Openhab (les noms des channels vus plus haut ), etc ...
    - Valider -> sauvegarder ! 
    - Essayer de contrôler votre objet connecté depuis openHab ! 

2. Installer mongodb sur votre machine !

## Pour exécuter la stack complète du projet :

1.  Récupérer l'ensemble du projet :
    - git clone https://gitlab.com/mbds/maslow/tpt-maslow.git
    - git submodule init
    - git submodule update

2.  Lancer la commande "mongod" pour démarrer la base de données
3.  Pour lancer le backend et le frontend consulter le README des repertoires backend-maslow et frontend-maslow


## Accéder aux différentes briques du projet :
- Backend : localhost:3000
- Frontend : localhost:8100
- Openhab : localhost:8080 | localhost:8443
- Base de données : localhost:27107

**L'équipe MASLOW**
    
